package com.test;

import org.junit.*;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FMSSomeTest {
	public Integer delay = 6000;
	public WebDriver driver;
	private static String SELENIUM_HUB_URL;
	private static String TARGET_SERVER_URL;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	public void waitForPageLoaded(WebDriver driver) throws InterruptedException {
		Thread.sleep(5000);
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 30);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.assertFalse(
					"Timeout waiting for Page Load Request to complete.", true);
		}

	}

	public void captureScreen() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();

		WebDriver augmentedDriver = new Augmenter().augment(driver);
		File screenshot = ((TakesScreenshot) augmentedDriver)
				.getScreenshotAs(OutputType.FILE);

		try {
			FileUtils.copyFile(
					screenshot,
					new File("target\\screenshots\\screenshot_"
							+ this.getClass().getName() + "_"
							+ dateFormat.format(date) + ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void dependableClick(WebDriver d, By by) {
		final int MAXIMUM_WAIT_TIME = 120;
		final int MAX_STALE_ELEMENT_RETRIES = 10;

		WebDriverWait wait = new WebDriverWait(d, MAXIMUM_WAIT_TIME);
		int retries = 0;
		while (true) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(by));
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				wait.until(ExpectedConditions.elementToBeClickable(by)).click();
				return;
			} catch (StaleElementReferenceException e) {
				if (retries < MAX_STALE_ELEMENT_RETRIES) {
					retries++;
					continue;
				} else {
					throw e;
				}
			}
		}
	}

	@Before
	public void setUp() throws Exception {
		SELENIUM_HUB_URL = "http://jenkins.qa.roc.local:4444/wd/hub";
		TARGET_SERVER_URL = "http://fms.qa.roc.local/";
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		driver = new RemoteWebDriver(new URL(SELENIUM_HUB_URL), capability);
		driver.get(TARGET_SERVER_URL);
		driver.manage().window().maximize();
	}

	@Test
	public void test2() throws Exception {
		// waitForPageLoaded(driver);
		driver.findElement(By.id("username_entry")).clear();
		driver.findElement(By.id("username_entry")).sendKeys("tse");
		driver.findElement(By.name("j_password")).clear();
		driver.findElement(By.name("j_password")).sendKeys("56TYivv46");
		driver.findElement(By.cssSelector("input.button")).click();
		waitForPageLoaded(driver);

		dependableClick(driver, By.linkText("Dashboards"));
		Thread.sleep(400);
		dependableClick(driver, By.linkText("Trip/Lockout"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Dashboards"));
		Thread.sleep(400);
		dependableClick(driver, By.linkText("IntelliTeam"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Dashboards"));
		Thread.sleep(400);
		dependableClick(driver, By.linkText("Communications Status"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Dashboards"));
		Thread.sleep(400);
		dependableClick(driver, By.linkText("Battery Status"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Dashboards"));
		Thread.sleep(400);
		dependableClick(driver, By.linkText("Software Versions"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Alerts"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Alert History"));
		Thread.sleep(delay);
		dependableClick(driver, By.linkText("Alert Snapshot"));
		waitForPageLoaded(driver);
		dependableClick(driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[4]/a/span"));
		Thread.sleep(delay);
		dependableClick(driver, By.linkText("Retrieval History"));
		waitForPageLoaded(driver);
		Thread.sleep(delay);
		dependableClick(driver, By.linkText("DNP Points"));
		waitForPageLoaded(driver);
		dependableClick(driver, By.linkText("Devices"));
		dependableClick(driver,
				By.xpath("/html/body/div[20]/div[2]/ul/li[780]/label"));
		dependableClick(driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT']/a/label"));

		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[7]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[8]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[9]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[18]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[17]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[22]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[23]/div/div"));
		dependableClick(
				driver,
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[24]/div/div"));
		dependableClick(
				driver,
				By.xpath("/html/body/div[4]/div[3]/div/div/div/div/div/table/tbody/tr/td/form/button[1]"));
		waitForPageLoaded(driver);
		Thread.sleep(delay);

		/*
		 * // Reports dependableClick(driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/a/span[1]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/ul/li[1]/a/span"
		 * )); dependableClick( driver, By.xpath(
		 * "html/body/div[4]/div[3]/div/div/div/div/div/fieldset/div/table/tbody/tr/td/form/div/div[1]/div[2]/button[1]"
		 * )); Thread.sleep(delay); String parentWindowHandler =
		 * driver.getWindowHandle(); // Store your // parent window String
		 * subWindowHandler = null;
		 * 
		 * Set<String> handles = driver.getWindowHandles(); // get all window //
		 * handles Iterator<String> iterator = handles.iterator(); while
		 * (iterator.hasNext()) { subWindowHandler = iterator.next(); }
		 * driver.switchTo().window(subWindowHandler); // switch to popup window
		 * // perform operations on // popup
		 * 
		 * dependableClick( driver,
		 * By.xpath("html/body/div[9]/div[2]/form/div[2]/div/div[1]/div[2]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[9]/div[2]/form/div[2]/div/div[2]/div[2]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[9]/div[2]/form/div[2]/div/div[3]/div[2]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[9]/div[2]/form/div[2]/div/div[4]/div[2]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[9]/div[2]/form/div[2]/div/div[5]/div[2]"));
		 * 
		 * dependableClick(driver,
		 * By.xpath("html/body/div[9]/div[2]/form/button[1]"));
		 * 
		 * driver.switchTo().window(parentWindowHandler); // switch back to
		 * parent // window waitForPageLoaded(driver);
		 * 
		 * dependableClick(driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/a/span[1]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/ul/li[3]/a/span"
		 * )); dependableClick( driver, By.xpath(
		 * "html/body/div[4]/div[3]/div/div/div/div/div/fieldset/div/table/tbody/tr/td/form/div/div[1]/div[2]/button[1]"
		 * )); Thread.sleep(delay); parentWindowHandler =
		 * driver.getWindowHandle(); // Store your parent // window
		 * subWindowHandler = null;
		 * 
		 * handles = driver.getWindowHandles(); // get all window handles
		 * iterator = handles.iterator(); while (iterator.hasNext()) {
		 * subWindowHandler = iterator.next(); }
		 * driver.switchTo().window(subWindowHandler); // switch to popup window
		 * // perform operations on // popup
		 * 
		 * dependableClick(driver,
		 * By.xpath("html/body/div[9]/div[2]/form/button[1]"));
		 * driver.switchTo().window(parentWindowHandler); // switch back to
		 * parent // window waitForPageLoaded(driver);
		 * 
		 * waitForPageLoaded(driver); dependableClick(driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/a/span[1]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/ul/li[5]/a/span"
		 * )); dependableClick( driver, By.xpath(
		 * "html/body/div[4]/div[3]/div/div/div/div/div/fieldset/div/table/tbody/tr/td/form/div/div[1]/div[2]/button[1]"
		 * )); Thread.sleep(delay); Thread.sleep(delay); parentWindowHandler =
		 * driver.getWindowHandle(); // Store your parent // window
		 * subWindowHandler = null;
		 * 
		 * handles = driver.getWindowHandles(); // get all window handles
		 * iterator = handles.iterator(); while (iterator.hasNext()) {
		 * subWindowHandler = iterator.next(); }
		 * driver.switchTo().window(subWindowHandler); // switch to popup window
		 * // perform operations on // popup dependableClick(driver,
		 * By.xpath("html/body/div[9]/div[2]/form/button[1]"));
		 * driver.switchTo().window(parentWindowHandler); // switch back to
		 * parent // window waitForPageLoaded(driver); dependableClick(driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/a/span[1]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/ul/li[7]/a/span"
		 * )); dependableClick( driver, By.xpath(
		 * "html/body/div[4]/div[3]/div/div/div/div/div/fieldset/div/table/tbody/tr/td/form/div/div[1]/div[2]/button[1]"
		 * ));
		 * 
		 * Thread.sleep(delay); parentWindowHandler = driver.getWindowHandle();
		 * // Store your parent // window subWindowHandler = null;
		 * 
		 * handles = driver.getWindowHandles(); // get all window handles
		 * iterator = handles.iterator(); while (iterator.hasNext()) {
		 * subWindowHandler = iterator.next(); }
		 * driver.switchTo().window(subWindowHandler); // switch to popup window
		 * // perform operations on // popup dependableClick(driver,
		 * By.xpath("html/body/div[9]/div[2]/form/button[1]"));
		 * driver.switchTo().window(parentWindowHandler); // switch back to
		 * parent // window waitForPageLoaded(driver);
		 * 
		 * waitForPageLoaded(driver); dependableClick(driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/a/span[1]"));
		 * dependableClick( driver,
		 * By.xpath("html/body/div[2]/div/form[1]/div/ul/li[6]/ul/li[9]/a/span"
		 * )); dependableClick( driver, By.xpath(
		 * "html/body/div[4]/div[3]/div/div/div/div/div/fieldset/div/table/tbody/tr/td/form/div/div[1]/div[2]/button[1]"
		 * ));
		 * 
		 * Thread.sleep(delay); parentWindowHandler = driver.getWindowHandle();
		 * // Store your parent // window subWindowHandler = null;
		 * 
		 * handles = driver.getWindowHandles(); // get all window handles
		 * iterator = handles.iterator(); while (iterator.hasNext()) {
		 * subWindowHandler = iterator.next(); }
		 * driver.switchTo().window(subWindowHandler); // switch to popup window
		 * // perform operations on // popup dependableClick(driver,
		 * By.xpath("html/body/div[9]/div[2]/form/button[2]"));
		 * driver.switchTo().window(parentWindowHandler); // switch back to
		 * parent // window waitForPageLoaded(driver);
		 */
		dependableClick(driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/a/span[1]"));
		Thread.sleep(300);
		dependableClick(
				driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/ul/li[1]/a/span"));
		waitForPageLoaded(driver);
		dependableClick(driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/a/span[1]"));
		Thread.sleep(300);
		dependableClick(
				driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/ul/li[3]/a/span"));
		waitForPageLoaded(driver);
		dependableClick(driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/a/span[1]"));
		Thread.sleep(300);
		dependableClick(
				driver,
				By.xpath("html/body/div[2]/div/form[1]/div/ul/li[7]/ul/li[9]/a/span"));
		waitForPageLoaded(driver);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			Assert.fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
