package com.test;

import org.junit.*;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FMSSomeTestFF {
	public Integer delay = 6000;
	public WebDriver driver;
	private static String SELENIUM_HUB_URL;
	private static String TARGET_SERVER_URL;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	public void waitForPageLoaded(WebDriver driver) {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 30);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.assertFalse(
					"Timeout waiting for Page Load Request to complete.", true);
		}
	}

	public void captureScreen() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();

		WebDriver augmentedDriver = new Augmenter().augment(driver);
		File screenshot = ((TakesScreenshot) augmentedDriver)
				.getScreenshotAs(OutputType.FILE);

		try {
			FileUtils.copyFile(
					screenshot,
					new File("target\\screenshots\\screenshot_"
							+ this.getClass().getName() + "_"
							+ dateFormat.format(date) + ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Before
	public void setUp() throws Exception {
		SELENIUM_HUB_URL = "http://jenkins.qa.roc.local:4444/wd/hub";
		TARGET_SERVER_URL = "http://fms.qa.roc.local/";
		DesiredCapabilities capability = DesiredCapabilities.htmlUnit();
		driver = new RemoteWebDriver(new URL(SELENIUM_HUB_URL), capability);
		driver.get(TARGET_SERVER_URL);
		driver.manage().window().maximize();
	}

	@Test
	public void test2() throws Exception {
		waitForPageLoaded(driver);
		driver.findElement(By.id("username_entry")).clear();
		driver.findElement(By.id("username_entry")).sendKeys("tse");
		driver.findElement(By.name("j_password")).clear();
		driver.findElement(By.name("j_password")).sendKeys("56TYivv46");
		driver.findElement(By.cssSelector("input.button")).click();
		waitForPageLoaded(driver);

		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/ul/li/a/span"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/ul/li[3]/a/span"))
				.click();
		waitForPageLoaded(driver);

		driver.findElement(By.linkText("Dashboards")).click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Communications Status")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Battery Status")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt61']/ul/li[2]/ul/li[9]/a/span"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Alerts")).click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Alert History")).click();
		Thread.sleep(delay);
		driver.findElement(By.linkText("Alert Snapshot")).click();
		waitForPageLoaded(driver);
		Thread.sleep(delay);

		driver.findElement(
				By.xpath("//div[@id='menus:j_idt69']/ul/li[4]/a/span")).click();
		Thread.sleep(delay);
		driver.findElement(By.linkText("Retrieval History")).click();
		waitForPageLoaded(driver);
		Thread.sleep(delay);

		driver.findElement(By.linkText("DNP Points")).click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Devices")).click();
		driver.findElement(
				By.xpath("/html/body/div[20]/div[2]/ul/li[780]/label")).click();
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT']/a/label"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[7]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[8]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[9]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[18]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[17]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[22]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[23]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='dnpMenuPanel:selectedAnalogDT_panel']/div[2]/ul/li[24]/div/div"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(By.id("dnpMenuPanel:j_idt394")).click();
		waitForPageLoaded(driver);
		Thread.sleep(delay);
		// Reports
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt23']/ul/li[6]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt23']/ul/li[6]/ul/li/a/span"))
				.click();

		driver.findElement(
				By.id("j_idt191:j_idt207:ReportTable:j_idt213:j_idt37"))
				.click();
		Thread.sleep(delay);
		String parentWindowHandler = driver.getWindowHandle(); // Store your
																// parent window
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // get all window
															// handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
													// perform operations on
													// popup

		driver.findElement(
				By.xpath("//div[@id='j_idt40:j_idt45:0:j_idt46']/div[2]"))
				.click();
		driver.findElement(
				By.xpath("//div[@id='j_idt40:j_idt45:1:j_idt46']/div[2]"))
				.click();
		driver.findElement(
				By.xpath("//div[@id='j_idt40:j_idt45:2:j_idt46']/div[2]"))
				.click();
		driver.findElement(
				By.xpath("//div[@id='j_idt40:j_idt45:3:j_idt46']/div[2]"))
				.click();
		driver.findElement(
				By.xpath("//div[@id='j_idt40:j_idt45:4:j_idt46']/div[2]"))
				.click();
		driver.findElement(By.id("j_idt40:RunButton")).click();
		driver.switchTo().window(parentWindowHandler); // switch back to parent
														// window
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/ul/li[3]/a/span"))
				.click();

		driver.findElement(
				By.id("j_idt191:j_idt207:ReportTable:j_idt213:j_idt37"))
				.click();
		Thread.sleep(delay);
		parentWindowHandler = driver.getWindowHandle(); // Store your parent
														// window
		subWindowHandler = null;

		handles = driver.getWindowHandles(); // get all window handles
		iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
													// perform operations on
													// popup

		driver.findElement(By.id("j_idt40:RunButton")).click();
		driver.switchTo().window(parentWindowHandler); // switch back to parent
														// window
		waitForPageLoaded(driver);

		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/ul/li[5]/a/span"))
				.click();
		driver.findElement(
				By.id("j_idt191:j_idt207:ReportTable:j_idt213:j_idt37"))
				.click();
		Thread.sleep(delay);
		parentWindowHandler = driver.getWindowHandle(); // Store your parent
														// window
		subWindowHandler = null;

		handles = driver.getWindowHandles(); // get all window handles
		iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
													// perform operations on
													// popup
		driver.findElement(By.id("j_idt40:RunButton")).click();
		driver.switchTo().window(parentWindowHandler); // switch back to parent
														// window
		waitForPageLoaded(driver);

		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/ul/li[5]/a/span"))
				.click();
		driver.findElement(
				By.id("j_idt191:j_idt207:ReportTable:j_idt213:j_idt37"))
				.click();
		Thread.sleep(delay);
		parentWindowHandler = driver.getWindowHandle(); // Store your parent
														// window
		subWindowHandler = null;

		handles = driver.getWindowHandles(); // get all window handles
		iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
													// perform operations on
													// popup
		driver.findElement(By.id("j_idt40:RunButton")).click();
		driver.switchTo().window(parentWindowHandler); // switch back to parent
														// window
		waitForPageLoaded(driver);

		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/ul/li[7]/a/span"))
				.click();
		driver.findElement(
				By.id("j_idt191:j_idt207:ReportTable:j_idt213:j_idt37"))
				.click();
		Thread.sleep(delay);
		parentWindowHandler = driver.getWindowHandle(); // Store your parent
														// window
		subWindowHandler = null;

		handles = driver.getWindowHandles(); // get all window handles
		iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
													// perform operations on
													// popup
		driver.findElement(By.id("j_idt40:RunButton")).click();
		driver.switchTo().window(parentWindowHandler); // switch back to parent
														// window
		waitForPageLoaded(driver);

		waitForPageLoaded(driver);
		driver.findElement(By.linkText("Reports")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[6]/ul/li[9]/a/span"))
				.click();
		waitForPageLoaded(driver);

		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[7]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt70']/ul/li[7]/ul/li/a/span"))
				.click();
		waitForPageLoaded(driver);
		driver.findElement(
				By.xpath("//div[@id='menus:j_idt22']/ul/li[7]/a/span")).click();
		waitForPageLoaded(driver);
		driver.findElement(By.linkText("About")).click();
		waitForPageLoaded(driver);
		waitForPageLoaded(driver);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			Assert.fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
